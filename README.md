# Websites based on the Pico CMS

Pico is a content management system (CMS). You can learn more about Pico at [picocms.org](https://picocms.org).

## Why use Pico?

- Open-source: [github.com/picocms/Pico](https://github.com/picocms/Pico "Pico's GitHub Respository")
- Secure and *Very Fast*
- Portable:
  + Small (1.19 MB zipped)
  + No database: content stored as text, [Markdown](https://daringfireball.net/projects/markdown "Daring FireBall: Markdown Project")
  + [Minimal PHP requirements](https://picocms.org/docs/#install) supported by most commercial hosts
- Flexible
  + Pico uses [Twig](https://twig.symfony.com "Twig Website"). It can adopt virtually any HTML template.
  + Pico has a [plugin architecture](https://picocms.org/docs/#plugins).

### Pico is Straightforward

Pico does not have a backend. The menu is generated based on the folder structure of the content files. Most webmasters, communications personnel, administrative professionals and others are already familiar with: desktop file operations and text editing - easing their adoption of Pico. 

The routine work of maintaining Pico's content involve the familiar operations:

- Adding and editing text files.
- Moving files between folders.
- Adding images, documents (and other assets) to a folder.

## Activities

These are the activities for this account.

### Themes

- Port HTML themes to Pico
- Add themes to Pico [Community Themes](https://picocms.org/themes)

### Plugins

- Create Plugins for Pico
- Update and document existing plugins

### Starter Websites

To make it easier for people to get started create Pico installations with:

- Plugins installed
- Themes installed and customised
- Settings configured
- Sample content
- Full documentation

### Desktop and Intranet Installations

Document and **automate** the process of setting up a desktop computer to run one or more Pico instances.

- Install and configure web server
- Install and configure PHP (including modules)
- Configure desktop
- Downloading websites

## Hire For

- Creating a Pico theme from HTML template
- Converting an existing website to Pico
- General **Webmastery**
  + Building a new website
  + Updating or fixing an existing website
  + Managing website content.

## Contact

**webmaster** *aht* **picowebsites** *doht* **com**
